//Configuración firebase

  //Import the functions you need from the SDKs you need
  import { initializeApp } from "https://www.gstatic.com/firebasejs/9.12.1/firebase-app.js";
  //import { getAnalytics } from "https://www.gstatic.com/firebasejs/9.12.1/firebase-analytics.js";
  import { getFirestore, doc, getDoc, getDocs, collection } from "https://www.gstatic.com/firebasejs/9.4.0/firebase-firestore.js";
  import { getDatabase,onValue,ref,set,child,get,update,remove} from "https://www.gstatic.com/firebasejs/9.12.1/firebase-database.js";
  import { getStorage, ref as refS, uploadBytes, getDownloadURL} from "https://www.gstatic.com/firebasejs/9.12.1/firebase-storage.js";

  // TODO: Add SDKs for Firebase products that you want to use
  // https://firebase.google.com/docs/web/setup#available-libraries

  // Your web app's Firebase configuration
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  const firebaseConfig = {
    apiKey: "AIzaSyDR6BRS3H2k9Hz0yG03U2LGOeAehm-_3Dg",
    authDomain: "paginaweb3-58833.firebaseapp.com",
    databaseURL: "https://paginaweb3-58833-default-rtdb.firebaseio.com",
    projectId: "paginaweb3-58833",
    storageBucket: "paginaweb3-58833.appspot.com",
    messagingSenderId: "310734732771",
    appId: "1:310734732771:web:cc154265f9b40fa86374d6",
    measurementId: "G-VBNJL2119Z"
  };

  // Initialize Firebase
  const app = initializeApp(firebaseConfig);
  const db = getDatabase();


    var btnInsertar = document.getElementById("btnInsertar");
    var btnBuscar = document.getElementById("btnBuscar");
    var btnActualizar = document.getElementById("btnActualizar");
    var btnBorrar = document.getElementById("btnBorrar");
    var btnTodos = document.getElementById("btnTodos");
    var caja = document.getElementById("Imagen");
    var btnLimpiar = document.getElementById('btnLimpiar');
    var archivo=document.getElementById("archivo")
    

// Insertar
    var idproducto = "";
    var nombre = "";
    var descripcion = "";
    var precio = "";
    var status="";
    var url="";
    var Imge="";

    function leerInputs(){
        idproducto=document.getElementById("idp").value;
        nombre=document.getElementById("nombre").value;
        descripcion=document.getElementById("Descripcion").value;
        precio=document.getElementById("Precio").value;
        status=document.getElementById("Status").value;
        Imge =document.getElementById("NombreImg").value;
        url=document.getElementById("url").value;
        

    }
    function insertDatos(){
        leerInputs();
        if ( idproducto==""|| nombre=="" || precio=="" || url=="") {
            alert("Datos faltantes");
        }
        else{
            set(ref(db,'productos/' + idproducto),{
                nombre: nombre,
                descripcion:descripcion,
                precio:precio,
                status:status,
                Imge:Imge,
                url:url})


            .then((docRef) => {
            alert("Registro exitoso");
            mostrarProductos();
            limpiar();
            })
            .catch((error) => {
            alert("Error en el registro")
            });
        }

    };
// mostrar datos
    function mostrarProductos(){
        const db = getDatabase();
        const dbRef = ref(db, 'productos');
        onValue(dbRef, (snapshot) => {
        caja.innerHTML=""
        snapshot.forEach((childSnapshot) => {
        const childKey = childSnapshot.key;
        const childData = childSnapshot.val();
        if(childData.status=="0"){
            caja.innerHTML= caja.innerHTML +"<section class='ProductosI'>" +
             
            "<img src='"+childData.url+"' alt=''>" +"<br>"+
            "<p style='font-size: larger;'>" + childData.nombre +"<br>"+"Precio: $"+childData.precio+ "<br>"+"Contiene: "+ childData.descripcion +"<br> Status:Disponible"+"</p>" + 
            "</section>" ;

        }
        if(childData.status=="1"){
            caja.innerHTML= caja.innerHTML +"<div class='ProductosI'>" +
             
            "<img src='"+childData.url+"' alt=''>" +"<br>"+
            "<p style='font-size: larger;'>" + childData.nombre +"<br>"+"Precio: $"+childData.precio+ "<br>"+"Contiene: "+ childData.descripcion +"<br> Status:No isponible"+"</p>" + 
            "</div>" ;

        }
            
        
        // ...
        });
        }, {
        onlyOnce: true
        });
    }
    function actualizar(){
        leerInputs();
        if ( idproducto==""|| nombre=="" || precio=="" || url=="") {
            alert("Datos faltantes");
        }
        else{
            update(ref(db,'productos/' + idproducto),{
                nombre: nombre,
                descripcion:descripcion,
                precio:precio,
                status:status,
                Imge:Imge,
                url:url
            }).then(()=>{
            alert("se realizo actualizacion");
            mostrarProductos();
            limpiar();
            })
            .catch(()=>{
            alert("Surgio un error " + error);
            });
        }
       
    }
    function escribirInpust(){
        document.getElementById("idp").value=idproducto;
        document.getElementById("nombre").value=nombre;
        document.getElementById("Descripcion").value=descripcion;
        document.getElementById("Precio").value=precio;
        document.getElementById("Status").value=status;
        document.getElementById("url").value=url;
        document.getElementById("NombreImg").value=Imge;
    }
    function borrar(){
        leerInputs();
        if ( idproducto=="") {
            alert("Datos faltantes");
        }
        else{
            update(ref(db, 'productos/' + idproducto),{
            status:"1"
            }).then(()=>{
                alert("Se deshabilitó");
                
                limpiar();
            }).catch(() => {
                alert("Surgio un error" + error);
            });
        }
        }
        
    function mostrarDatos(){
        leerInputs();
        
        const dbref = ref(db);
        get(child(dbref,'productos/'+ idproducto)).then((snapshot)=>{
        if(snapshot.exists()){
            nombre = snapshot.val().nombre;
            descripcion = snapshot.val().descripcion;
            precio = snapshot.val().precio;
            status=snapshot.val().status;
            Imge=snapshot.val().Imge;
            url=snapshot.val().url;
            Ar(url);
            escribirInpust();
        }
        else {

            alert("No existe");
        }
        }).catch((error)=>{
            alert("error buscar" + error);
        });
    }
    function Ar(url) {
        document.getElementById("Im").innerHTML="";
        document.getElementById("Im").innerHTML="<div class='imagen2'>"+"<img src='"+url+"' alt=''>" +"</div>";
    }
    function limpiar(){
        caja.innerHTML="";
        idproducto="";
        nombre="";
        descripcion="";
        precio="";
        status=1;
        Imge="";
        url="";
        escribirInpust();
    }
    btnInsertar.addEventListener('click',insertDatos);
    btnBuscar.addEventListener('click',mostrarDatos);
    btnActualizar.addEventListener('click',actualizar);
    btnBorrar.addEventListener('click',borrar);
    btnTodos.addEventListener('click', mostrarProductos);
    btnLimpiar.addEventListener('click', limpiar);
    archivo.addEventListener('change',cargarImagen);
   
async function cargarImagen(){
    const file= event.target.files[0];
    const name= event.target.files[0].name;
  
    const storage= getStorage();
    const storageRef= refS(storage, 'imagenes/' + name);
  
    await uploadBytes(storageRef, file).then((snapshot) => {
      
      document.getElementById('NombreImg').value=name;
        descargarImagen();
      alert('se cargo la imagen');
    });
  }
  
  async function descargarImagen(){
    archivo= document.getElementById('NombreImg').value;
    // Create a reference to the file we want to download
  const storage = getStorage();
  const starsRef = refS(storage, 'imagenes/' + archivo);
  
  // Get the download URL
  getDownloadURL(starsRef)
    .then((url) => {
    Ar(url)
     document.getElementById('url').value=url;
     document.getElementById('imagen').src=url;
    })
    .catch((error) => {
      // A full list of error codes is available at
      // https://firebase.google.com/docs/storage/web/handle-errors
      switch (error.code) {
        case 'storage/object-not-found':
          alert("No existe el archivo");
          break;
        case 'storage/unauthorized':
          alert("No tiene permisos");
          break;
        case 'storage/canceled':
          alert("No existe conexion con la base de datos")
          break;
  
        // ...
  
        case 'storage/unknown':
          alert("Ocurrio algo inesperado")
          break;
      }
    });
  }
