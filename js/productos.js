//Configuración firebase

  //Import the functions you need from the SDKs you need
  import { initializeApp } from "https://www.gstatic.com/firebasejs/9.12.1/firebase-app.js";
  //import { getAnalytics } from "https://www.gstatic.com/firebasejs/9.12.1/firebase-analytics.js";
  import { getFirestore, doc, getDoc, getDocs, collection } from "https://www.gstatic.com/firebasejs/9.4.0/firebase-firestore.js";
  import { getDatabase,onValue,ref,set,child,get,update,remove} from "https://www.gstatic.com/firebasejs/9.12.1/firebase-database.js";
  import { getStorage, ref as refS, uploadBytes, getDownloadURL} from "https://www.gstatic.com/firebasejs/9.12.1/firebase-storage.js";

  // TODO: Add SDKs for Firebase products that you want to use
  // https://firebase.google.com/docs/web/setup#available-libraries

  // Your web app's Firebase configuration
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  const firebaseConfig = {
    apiKey: "AIzaSyDR6BRS3H2k9Hz0yG03U2LGOeAehm-_3Dg",
    authDomain: "paginaweb3-58833.firebaseapp.com",
    databaseURL: "https://paginaweb3-58833-default-rtdb.firebaseio.com",
    projectId: "paginaweb3-58833",
    storageBucket: "paginaweb3-58833.appspot.com",
    messagingSenderId: "310734732771",
    appId: "1:310734732771:web:cc154265f9b40fa86374d6",
    measurementId: "G-VBNJL2119Z"
  };

  // Initialize Firebase
  const app = initializeApp(firebaseConfig);
  const db = getDatabase();
  var caja=document.getElementById('ProdP');


  function importe(){
    const db = getDatabase();
    const dbRef= ref(db, 'productos');
    onValue(dbRef,(snapshot) => {
        caja.innerHTML="";
        snapshot.forEach((childSnapshot) => {
         
            const childKey = childSnapshot.key;
            const childData = childSnapshot.val();
            if(childData.status=="0"){
            caja.innerHTML= caja.innerHTML +"<section class='ProductosI'>" +
             
            "<img src='"+childData.url+"' alt=''>" +"<br>"+
            "<p style='font-size: larger;'>" + childData.nombre +"<br>"+"Precio: $"+childData.precio+ "<br>"+"Contiene: "+ childData.descripcion +"</p>" + 
            "</section>" ;
        }
        });
        {
            onlyOnce: true
        }
    });
  }
window.onload(importe());
