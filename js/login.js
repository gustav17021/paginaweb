//Configuración firebase

  //Import the functions you need from the SDKs you need
  import { initializeApp } from "https://www.gstatic.com/firebasejs/9.12.1/firebase-app.js";
  //import { getAnalytics } from "https://www.gstatic.com/firebasejs/9.12.1/firebase-analytics.js";
  import{getAuth, signInWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/9.12.1/firebase-auth.js";
  import { getDatabase,onValue,ref,set,child,get,update,remove} from "https://www.gstatic.com/firebasejs/9.12.1/firebase-database.js";
  import { getStorage, ref as refS, uploadBytes, getDownloadURL} from "https://www.gstatic.com/firebasejs/9.12.1/firebase-storage.js";

  // TODO: Add SDKs for Firebase products that you want to use
  // https://firebase.google.com/docs/web/setup#available-libraries

  // Your web app's Firebase configuration
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  const firebaseConfig = {
    apiKey: "AIzaSyDR6BRS3H2k9Hz0yG03U2LGOeAehm-_3Dg",
    authDomain: "paginaweb3-58833.firebaseapp.com",
    databaseURL: "https://paginaweb3-58833-default-rtdb.firebaseio.com",
    projectId: "paginaweb3-58833",
    storageBucket: "paginaweb3-58833.appspot.com",
    messagingSenderId: "310734732771",
    appId: "1:310734732771:web:cc154265f9b40fa86374d6",
    measurementId: "G-VBNJL2119Z"
  };

  // Initialize Firebase
  const app = initializeApp(firebaseConfig);
  const db = getDatabase();


  var login=document.getElementById("Log");
    var correo = "";
    var pass = "";
   login.addEventListener('click',buscar);

    function leerInputsL(){
        correo=document.getElementById("user").value;
        pass=document.getElementById("pass").value;
    }
    
   
    function buscar(){

        leerInputsL();
        
        const auth = getAuth();
        signInWithEmailAndPassword(auth, correo, pass)
        .then((userCredential) => {
        // Signed in 
        const user = userCredential.user;
        location.href="/html/admin.html"
        // ...
        })
        .catch((error) => {
        location.href="/html/error.html";
        });
    }

